import io
import random
from functools import lru_cache

from PIL import Image
from fastapi import FastAPI, File
from pydantic import BaseModel, confloat

app = FastAPI()


class ModelAnswer(BaseModel):
    label: str
    confidence: confloat(ge=0.0, le=1.0)


class MockModel:
    def predict(self, image):
        label = random.randint(0, 4)
        conf = random.random() * hash(image)
        return label, conf


@lru_cache()
def get_model():
    return MockModel()


@app.post('/predict')
def predict(image: bytes = File(...)):
    model = get_model()
    image = Image.open(io.BytesIO(image)).convert("RGB")
    label, conf = model.predict(image)
    return ModelAnswer(
        label=str(label),
        confidence=conf,
    )
